import React, { useCallback } from 'react';
import http from './../http';

export interface Shop {
  id: number,
  title: string,
  liked: boolean,
  avatarUrl: string,
  description: string,
  rate: number,
  bonusPercents: string, //  TODO split to "from - to"
}

function ShopItem({ shop, onLike }: { shop: Shop, onLike: any }) {

  return (
    <div className="col-xl-4">
      <a href="shop_full.html" className="shop_item">
        <div className="shop_item__title">
          <h3>{shop.title}</h3>
          <div className="shop_item__title___right">
            <button onClick={onLike} className={`like${shop.liked ? " like_active" : ""}`}>
              <i className="fas fa-heart"></i>
            </button>
          </div>
        </div>
        <div className="shop_item__center">
          <div className="shop_item__center___avatar">
            <div className="shop_avatar">
              <img src={shop.avatarUrl} />s
              <span>{shop.rate}</span>
            </div>
          </div>
          <div className="shop_item__center___text">
            <p>{shop.description}</p>
          </div>
        </div>
        <div className="shop_item__bonus">
          <h3>{shop.bonusPercents}<small>%</small></h3>
          <span>Бонусов</span>
        </div>
      </a>
    </div>
  )
}

export const Shops = ({ shops }: { shops: Shop[] }) => {

  const sendLike = useCallback((e, id) => {
    e.preventDefault();
    http.post('', { id }).then(res => {
      console.log(res.data);
    })
  }, []);

  return (
    <div className="shop">
      <div className="container_fluid">
        <div className="row">
          {shops.map((shop: Shop) => (
            <ShopItem
              key={shop.id}
              onLike={(e: any) => sendLike(e, shop.id)}
              shop={shop} />
          ))}
        </div>
      </div>
    </div>
  )
}