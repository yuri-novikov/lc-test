import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/shops';
axios.defaults.headers.post['Content-Type'] = 'application/json';

export default axios;