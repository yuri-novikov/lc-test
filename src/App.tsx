import React, { useState, useEffect, useCallback, SyntheticEvent } from 'react';
import { Shops } from './Components/Shops';
import { Bar } from './Components/Bar';
import http from './http';

export const App = () => {
  const [shops, setShops] = useState([]);

  useEffect(() => {
    http.get('')
      .then(res => { setShops(res.data) })
      .catch(e => { console.log(e.message) });
  }, [])

  const applySort = useCallback(
    (e) => {
      const order = e.target.value;
      if (order === 'new') {
        setShops(shops => shops.map(shop => shop).sort((a, b) => b.id - a.id))
      }
      if (order === 'chosen') {
        setShops(shops => {
          const liked = shops.filter(shop => shop.liked)
          const unliked = shops.filter(shop => !shop.liked)

          return [
            ...liked,
            ...unliked
          ]
        })
      }
    },
    []
  )

  return (
    <>
      <Bar select={applySort} />
      <Shops shops={shops} />
    </>
  )
}