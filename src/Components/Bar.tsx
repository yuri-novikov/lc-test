import * as React from 'react';

export const Bar = ({ select }: { select: any }) => {
  return (
    <div className="content_bar">
      <div className="content_bar__title">
        <h2>Заведения</h2>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><a href="#">Home</a></li>
            <li className="breadcrumb-item active" aria-current="page">Library</li>
          </ol>
        </nav>
      </div>
      <div className="content_bar__sorting">
        Сортировка:
        <select onChange={select}>
          <option></option>
          <option value="chosen">Избраные</option>
          <option value="new">Новое</option>
        </select>
      </div>
    </div>
  )
}